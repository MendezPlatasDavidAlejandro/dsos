/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.actividadeseconomicas.service;


import com.mycompany.actividadeseconomicas.Entidadeconomica;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author JHONATAN
 */
@Stateless
@Path("EntidadEconomica")
public class EntidadeconomicaFacadeREST extends AbstractFacade<Entidadeconomica> {

    @PersistenceContext(unitName = "com.mycompany_ActividadesEconomicas_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public EntidadeconomicaFacadeREST() {
        super(Entidadeconomica.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Entidadeconomica entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Entidadeconomica entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }
    // Recuperar una entidad economica a traves del ID
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Entidadeconomica find(@PathParam("id") Integer id) {
        return super.find(id);
    }
    // Filtrar las entidades economicas a traves del tipo de actividad
    @GET
    @Path("/tipoActividad/{tipo}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Entidadeconomica> find(@PathParam("tipo") String tipoA) {
        return findByTipoA(tipoA);
    }
    // El filtrado por tipo de actividad y nombre
    @GET
    @Path("/tipoActividad/{tipo}/{nombre}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Entidadeconomica> find(@PathParam("tipo") String tipoA, @PathParam("nombre") String nombre) {
        return findByTipoNombre(tipoA,nombre);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Entidadeconomica> findAll() {
        return super.findAll();
    }
    
    // Paginación
    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Entidadeconomica> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }
    
    // Saber el número de entidades que se tienen en la BD
    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    
    /** Consultas **/

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public List<Entidadeconomica> findByTipoA(String tipoA) {
        return em.createQuery(
        "SELECT e FROM Entidadeconomica e where e.idtipo.descripcion LIKE :tipo")
        .setParameter("tipo", "%"+tipoA+"%")
        .getResultList();
    }
    /**
     * 
     * @param nombre Nombre de la unidad economica
     * @param tipoA Tipo de actividad economica 
     * @return  Lista de entidades economicas 
     */
    public List<Entidadeconomica> findByTipoNombre(String tipoA, String nombre) {
        return em.createQuery(
        "SELECT e FROM Entidadeconomica e where e.idtipo.descripcion LIKE :tipo and e.nomEstab LIKE :nombre")
        .setParameter("tipo", "%"+tipoA+"%")
         .setParameter("nombre", "%"+nombre+"%")
        .getResultList();
    }
    
}
