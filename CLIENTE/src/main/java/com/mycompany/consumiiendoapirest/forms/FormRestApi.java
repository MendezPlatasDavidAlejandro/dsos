/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.consumiiendoapirest.forms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

import java.util.List;
/**
 *
 * @author Jorge
 */
public final class FormRestApi extends javax.swing.JFrame {
    
    private HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
    
    private final Object[] calum = new Object[] {"Id","Id Ususuario","Post","Coementario"};
    private final DefaultTableModel model = new DefaultTableModel(calum, 0);
    
    public FormRestApi() {
        initComponents();
        final HttpRequest requestPosts =  HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://actividades-eco.herokuapp.com/app/EntidadEconomica"))
                .build();
        try {
            final HttpResponse <String> response = httpClient.send(requestPosts, HttpResponse.BodyHandlers.ofString());
            List<Post> posts = convertirObjeto(response.body(), new TypeReference<List<Post>>() {});
            
            posts.stream().forEach(item ->{
                model.addRow(new Object[] {
                    item.getId(),
                    item.getNom_estab(), 
                    item.getRaz_social(), 
                    item.getPer_ocu(),
                    item.getNomb_asent(),
                    item.getCod_postal(),
                    item.getEntidad(),
                    item.getMunicipio(),
                    item.getLocalidad(),
                    item.getTelefono(),
                    item.getCorreoelec(),
                    item.getWww(),
                    item.getLatitud(),
                    item.getLongitud(),
                    item.getFecha_alta()
                });  
            });
            
            this.JTbPosts.setModel(model);
            
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(FormRestApi.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
    
    final ObjectMapper mapper = new ObjectMapper();
    
    public <T> T convertirObjeto(final String json, final TypeReference<T> reference){
        try {
            return this.mapper.readValue(json, reference);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(FormRestApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        JTbPosts = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        JTbPosts.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        JTbPosts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(JTbPosts);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormRestApi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormRestApi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormRestApi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormRestApi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormRestApi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable JTbPosts;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
