package com.mycompany.actividadeseconomicas;

import com.mycompany.actividadeseconomicas.Tipoactividad;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-07-15T21:04:14")
@StaticMetamodel(Entidadeconomica.class)
public class Entidadeconomica_ { 

    public static volatile SingularAttribute<Entidadeconomica, Double> latitud;
    public static volatile SingularAttribute<Entidadeconomica, String> fechaAlta;
    public static volatile SingularAttribute<Entidadeconomica, String> municipio;
    public static volatile SingularAttribute<Entidadeconomica, String> perOcu;
    public static volatile SingularAttribute<Entidadeconomica, String> razSocial;
    public static volatile SingularAttribute<Entidadeconomica, Tipoactividad> idtipo;
    public static volatile SingularAttribute<Entidadeconomica, String> nomEstab;
    public static volatile SingularAttribute<Entidadeconomica, Double> longitud;
    public static volatile SingularAttribute<Entidadeconomica, String> nombAsent;
    public static volatile SingularAttribute<Entidadeconomica, String> www;
    public static volatile SingularAttribute<Entidadeconomica, String> entidad;
    public static volatile SingularAttribute<Entidadeconomica, String> localidad;
    public static volatile SingularAttribute<Entidadeconomica, Integer> id;
    public static volatile SingularAttribute<Entidadeconomica, String> telefono;
    public static volatile SingularAttribute<Entidadeconomica, Integer> codPostal;
    public static volatile SingularAttribute<Entidadeconomica, String> correoelec;

}