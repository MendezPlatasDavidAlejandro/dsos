package com.mycompany.actividadeseconomicas;

import com.mycompany.actividadeseconomicas.Entidadeconomica;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-07-15T21:04:14")
@StaticMetamodel(Tipoactividad.class)
public class Tipoactividad_ { 

    public static volatile SingularAttribute<Tipoactividad, String> descripcion;
    public static volatile SingularAttribute<Tipoactividad, Integer> idtipo;
    public static volatile CollectionAttribute<Tipoactividad, Entidadeconomica> entidadeconomicaCollection;

}