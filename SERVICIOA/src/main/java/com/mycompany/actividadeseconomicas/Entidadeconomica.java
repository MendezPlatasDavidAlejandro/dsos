/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.actividadeseconomicas;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JHONATAN
 */
@Entity
@Table(name = "entidadeconomica")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entidadeconomica.findAll", query = "SELECT e FROM Entidadeconomica e")
    , @NamedQuery(name = "Entidadeconomica.findById", query = "SELECT e FROM Entidadeconomica e WHERE e.id = :id")
    , @NamedQuery(name = "Entidadeconomica.findByNomEstab", query = "SELECT e FROM Entidadeconomica e WHERE e.nomEstab = :nomEstab")
    , @NamedQuery(name = "Entidadeconomica.findByRazSocial", query = "SELECT e FROM Entidadeconomica e WHERE e.razSocial = :razSocial")
    , @NamedQuery(name = "Entidadeconomica.findByPerOcu", query = "SELECT e FROM Entidadeconomica e WHERE e.perOcu = :perOcu")
    , @NamedQuery(name = "Entidadeconomica.findByNombAsent", query = "SELECT e FROM Entidadeconomica e WHERE e.nombAsent = :nombAsent")
    , @NamedQuery(name = "Entidadeconomica.findByCodPostal", query = "SELECT e FROM Entidadeconomica e WHERE e.codPostal = :codPostal")
    , @NamedQuery(name = "Entidadeconomica.findByEntidad", query = "SELECT e FROM Entidadeconomica e WHERE e.entidad = :entidad")
    , @NamedQuery(name = "Entidadeconomica.findByMunicipio", query = "SELECT e FROM Entidadeconomica e WHERE e.municipio = :municipio")
    , @NamedQuery(name = "Entidadeconomica.findByLocalidad", query = "SELECT e FROM Entidadeconomica e WHERE e.localidad = :localidad")
    , @NamedQuery(name = "Entidadeconomica.findByTelefono", query = "SELECT e FROM Entidadeconomica e WHERE e.telefono = :telefono")
    , @NamedQuery(name = "Entidadeconomica.findByCorreoelec", query = "SELECT e FROM Entidadeconomica e WHERE e.correoelec = :correoelec")
    , @NamedQuery(name = "Entidadeconomica.findByWww", query = "SELECT e FROM Entidadeconomica e WHERE e.www = :www")
    , @NamedQuery(name = "Entidadeconomica.findByLatitud", query = "SELECT e FROM Entidadeconomica e WHERE e.latitud = :latitud")
    , @NamedQuery(name = "Entidadeconomica.findByLongitud", query = "SELECT e FROM Entidadeconomica e WHERE e.longitud = :longitud")
    , @NamedQuery(name = "Entidadeconomica.findByFechaAlta", query = "SELECT e FROM Entidadeconomica e WHERE e.fechaAlta = :fechaAlta")})
public class Entidadeconomica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nom_estab")
    private String nomEstab;
    @Column(name = "raz_social")
    private String razSocial;
    @Column(name = "per_ocu")
    private String perOcu;
    @Column(name = "nomb_asent")
    private String nombAsent;
    @Column(name = "cod_postal")
    private Integer codPostal;
    @Column(name = "entidad")
    private String entidad;
    @Column(name = "municipio")
    private String municipio;
    @Column(name = "localidad")
    private String localidad;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "correoelec")
    private String correoelec;
    @Column(name = "www")
    private String www;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitud")
    private Double latitud;
    @Column(name = "longitud")
    private Double longitud;
    @Column(name = "fecha_alta")
    private String fechaAlta;
    @JoinColumn(name = "idtipo", referencedColumnName = "idtipo")
    @ManyToOne
    private Tipoactividad idtipo;

    public Entidadeconomica() {
    }

    public Entidadeconomica(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomEstab() {
        return nomEstab;
    }

    public void setNomEstab(String nomEstab) {
        this.nomEstab = nomEstab;
    }

    public String getRazSocial() {
        return razSocial;
    }

    public void setRazSocial(String razSocial) {
        this.razSocial = razSocial;
    }

    public String getPerOcu() {
        return perOcu;
    }

    public void setPerOcu(String perOcu) {
        this.perOcu = perOcu;
    }

    public String getNombAsent() {
        return nombAsent;
    }

    public void setNombAsent(String nombAsent) {
        this.nombAsent = nombAsent;
    }

    public Integer getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(Integer codPostal) {
        this.codPostal = codPostal;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreoelec() {
        return correoelec;
    }

    public void setCorreoelec(String correoelec) {
        this.correoelec = correoelec;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public String getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Tipoactividad getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(Tipoactividad idtipo) {
        this.idtipo = idtipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entidadeconomica)) {
            return false;
        }
        Entidadeconomica other = (Entidadeconomica) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.actividadeseconomicas.Entidadeconomica[ id=" + id + " ]";
    }
    
}
